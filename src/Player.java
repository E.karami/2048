import java.util.ArrayList;
import java.io.*;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Player {
    private int[][] Table;
    private int[][] prevTable;
    private int maxPoint;
    private int point;
    private int cachedPoint;
    public String PlayerName;


    /**
     * adding a amount to point
     * @param cachedPoint an number
     */
    private void addCachedPoint(int cachedPoint) {
        this.cachedPoint += cachedPoint;
    }

    /**
     * resetting the cached point in a move
     */
    private void resetCachedPoint() {
        this.cachedPoint = 0;
    }

    /**
     *  constructor
     */
    public Player(String PlayerName) {
        this.Table=new int[4][4];
        this.prevTable=new int[4][4];
        this.PlayerName=PlayerName;
        this.point=0;
        this.cachedPoint=0;
        this.maxPoint=extractMaxPoint();
    }

    /**
     *  constructor
     */
    public Player(String PlayerName,int[][] array,int[][] prevArray,int point,int cachedPoint) {
        this.Table=new int[4][4];
        for(int i=0;i<4;i++){
            for (int j=0;j<4;j++){
                this.Table[i][j]=array[i][j];
            }
        }
        this.prevTable=new int[4][4];
        for(int i=0;i<4;i++){
            for (int j=0;j<4;j++){
                this.prevTable[i][j]=prevArray[i][j];
            }
        }
        this.PlayerName=PlayerName;
        this.point=point;
        this.cachedPoint=cachedPoint;
        this.maxPoint=extractMaxPoint();
    }

    /**
     * this function is for play game...
     */
    public void start(char charLoad){
        if(charLoad!='l'){
            randomFill();
            randomFill();
            updatePrevTable();
        }
        Scanner in;
        char character;
        boolean flag=false;
        boolean prevFlag=false;
        do{
            in=new Scanner(System.in);
            cls();
            printHeader();
            print();
            helpSectionMessage();
            char validation;
            try{
                validation=in.nextLine().charAt(0);
            }catch (Exception ex){
                validation='m';
            }
            if(validation=='w'||validation=='s'||validation=='a'||validation=='d'
            ||validation=='z'||validation=='q'){
                character=validation;
            }else{
                character='m';
            }
            switch (character){
                case 'w':{
                    if(!prevFlag){
                        resetCachedPoint();
                    }
                    actionUp();
                    updatePoint();
                    updateMaxPoint();
                    if(checkWin()){
                        cls();
                        writeMaxPoint();
                        printHeader();
                        print();
                        winMessage();
                        flag=true;
                    }
                    else if(checkOver()){
                        cls();
                        writeMaxPoint();
                        printHeader();
                        print();
                        gameOverMessage();
                        flag=true;
                    } else if(isPrevLikeRecent()){
                        prevFlag=true;
                        //nothing
                    }else{
                        prevFlag=false;
                        randomFill();
                        updatePrevTable();
                    }
                }
                break;
                case 's':{
                    if(!prevFlag){
                        resetCachedPoint();
                    }
                    actionDown();
                    updatePoint();
                    updateMaxPoint();
                    if(checkOver()){
                        cls();
                        writeMaxPoint();
                        printHeader();
                        print();
                        gameOverMessage();
                        flag=true;
                    }else if(checkWin()){
                        cls();
                        writeMaxPoint();
                        printHeader();
                        print();
                        winMessage();
                        flag=true;
                    }else if(isPrevLikeRecent()){
                        prevFlag=true;
                        //nothing
                    }else{
                        prevFlag=false;
                        randomFill();
                        updatePrevTable();
                    }
                }
                break;
                case 'a':{
                    if(!prevFlag){
                        resetCachedPoint();
                    }
                    actionLeft();
                    updatePoint();
                    updateMaxPoint();
                    if(checkOver()){
                        cls();
                        writeMaxPoint();
                        printHeader();
                        print();
                        gameOverMessage();
                        flag=true;
                    }else if(checkWin()){
                        cls();
                        writeMaxPoint();
                        printHeader();
                        print();
                        winMessage();
                        flag=true;
                    }else if(isPrevLikeRecent()){
                        prevFlag=true;
                        //nothing
                    }else{
                        prevFlag=false;
                        randomFill();
                        updatePrevTable();
                    }
                }
                break;
                case 'd':{
                    if(!prevFlag){
                        resetCachedPoint();
                    }
                    actionRight();
                    updatePoint();
                    updateMaxPoint();
                    if(checkOver()){
                        cls();
                        writeMaxPoint();
                        printHeader();
                        print();
                        gameOverMessage();
                        flag=true;
                    }else if(checkWin()){
                        cls();
                        writeMaxPoint();
                        printHeader();
                        print();
                        winMessage();
                        flag=true;
                    }else if(isPrevLikeRecent()){
                        prevFlag=true;
                        //nothing
                    }else{
                        prevFlag=false;
                        randomFill();
                        updatePrevTable();
                    }
                }
                break;
                case 'z':{
                    cls();
                    updateMaxPoint();
                    writeMaxPoint();
                    System.out.println("please enter save name (don't use @ * # ...) : ");
                    String saveName=in.next();
                    save(saveName);
                }
                break;
                case 'q':{
                    updateMaxPoint();
                    writeMaxPoint();
                    flag=true;
                }
                break;
                default:{
                    System.out.println();
                }
            }


        }while (character!='q'&&!flag);
    }

    /**
     * after each round check for game over
     * @return returns true if player lost else returns false
     */
    private boolean checkOver() {
         for(int i=0;i<4;i++){
             for(int j=0;j<4;j++){
                 if(this.Table[i][j]==0){
                     return false;
                 }else{
                     if(j-1>=0){
                         if(this.Table[i][j]==this.Table[i][j-1]){
                             return false;
                         }
                     }
                     if(j+1<4){
                         if(this.Table[i][j]==this.Table[i][j+1]){
                             return false;
                         }
                     }
                     if(i-1>=0){
                         if(this.Table[i][j]==this.Table[i-1][j]){
                             return false;
                         }
                     }
                     if(i+1<4){
                         if(this.Table[i][j]==this.Table[i+1][j]){
                             return false;
                         }
                     }
                 }
             }
         }
         return true;
    }

    /**
     * this function checks player won or not
     * @return  returns true if player won else returns false
     */
    private boolean checkWin(){
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                if(this.Table[i][j]==2048){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *    for print board
     */
    private void print() {
        int i=0;
        int j=0;
        int counter1;
        int counter2;
        for(counter1=1;counter1<=2*4+1;counter1++){
            if(counter1==1){
                for(counter2=1;counter2<=5*4+1;counter2++)
                {
                    if(counter2==1){
                        System.out.print("  "+"\u250c");
                    }else{
                        if(counter2<5*4+1){
                            switch (counter2%5){
                                case 1:System.out.print("\u252c");
                                break;
                                default:System.out.print("\u2501");
                                break;
                            }
                        }else{
                            System.out.print("\u2510");
                        }
                    }
                }
                System.out.println();
            }else {
                if(counter1<2*4+1) {
                    switch (counter1%2){
                        case 0:{
                            System.out.print("  ");
                            for(counter2=1;counter2<=5*4+1;)
                            {
                                switch (counter2%5){
                                    case 1:{
                                        System.out.print("\u2502");
                                        counter2 += 1;
                                    }
                                    break;
                                    case 2 :{
                                        if(this.Table[i][j]>=1000){
                                            System.out.print(this.Table[i][j]);
                                            j++;
                                            if(j==4){
                                                j=0;
                                                i++;
                                            }
                                            counter2+=4;
                                        }else{
                                            System.out.print(" ");
                                            counter2+=1;
                                        }
                                    }
                                    break;
                                    case 3 :{
                                        if(this.Table[i][j]<1000&&this.Table[i][j]>=100){
                                            System.out.print(this.Table[i][j]);
                                            j++;
                                            if(j==4){
                                                j=0;
                                                i++;
                                            }
                                            counter2+=3;
                                        }else{
                                            System.out.print(" ");
                                            counter2+=1;
                                        }
                                    }
                                    break;
                                    case 4 :{
                                        if(this.Table[i][j]<100&&this.Table[i][j]>=10)
                                        {
                                            System.out.print(this.Table[i][j]);
                                            j++;
                                            if(j==4){
                                                j=0;
                                                i++;
                                            }
                                            counter2+=2;
                                        }else{
                                            System.out.print(" ");
                                            counter2+=1;
                                        }
                                    }
                                    break;
                                    default :{
                                        if(this.Table[i][j]<10&&this.Table[i][j]>0) {
                                            System.out.print(this.Table[i][j]);
                                            j++;
                                            if(j==4){
                                                j=0;
                                                i++;
                                            }
                                            counter2+=1;
                                        }else if(this.Table[i][j]==0){
                                            System.out.print(" ");
                                            j++;
                                            if(j==4){
                                                j=0;
                                                i++;
                                            }
                                            counter2+=1;
                                        }
                                    }
                                    break;
                                }
                            }
                            System.out.println();
                        }
                        break;
                        case 1:{
                            System.out.print("  ");
                            for(counter2=1;counter2<=5*4+1;counter2++){
                                if(counter2==1){
                                    System.out.print("\u2520");
                                }else{
                                    if(counter2<5*4+1){
                                        switch (counter2%5){
                                            case 1:
                                                System.out.print("\u253c");
                                                break;
                                            default:
                                                System.out.print("\u2501");
                                                break;
                                        }
                                    }else{
                                        System.out.print("\u2528");
                                    }
                                }
                            }
                            System.out.println();
                        }
                        break;
                    }
                }else{
                    System.out.print("  ");
                    for(counter2=1;counter2<=5*4+1;counter2++){
                        if(counter2==1){
                            System.out.print("\u2514");
                        }else{
                            if(counter2<5*4+1){
                                switch (counter2%5){
                                    case 1:
                                        System.out.print("\u2534");
                                        break;
                                    default:
                                        System.out.print("\u2501");
                                        break;
                                }
                            }else{
                                System.out.print("\u2518");
                            }
                        }
                    }
                }
            }
        }
        System.out.println();
    }
    /**
     *    apply up (w) action
     */
    private void actionUp() {
        for(int i=0;i<4;i++){
            int k=0;
            int j=1;
            while(j<4&&k<4){
                if(this.Table[j][i]==0){
                    j++;
                }else if(this.Table[j][i]!=0){
                    if(this.Table[k][i]==0){
                        this.Table[k][i]=this.Table[j][i];
                        this.Table[j][i]=0;
                        j++;
                    }else if(this.Table[k][i]==this.Table[j][i]){
                        this.Table[k][i]+=this.Table[j][i];
                        addCachedPoint(this.Table[k][i]);
                        this.Table[j][i]=0;
                        k++;
                        j++;
                    }else if(this.Table[k][i]!=this.Table[j][i]){
                        if(k+1!=j){
                            this.Table[k+1][i]=this.Table[j][i];
                            this.Table[j][i]=0;
                        }
                        k++;
                        j++;
                    }
                }
            }
        }
    }

    /**
     *    apply down (s) action
     */
    private void actionDown() {
        for(int i=0;i<4;i++){
            int k=3;
            int j=2;
            while(j>=0&&k>=0){
                if(this.Table[j][i]==0){
                    j--;
                }else if(this.Table[j][i]!=0){
                    if(this.Table[k][i]==0){
                        this.Table[k][i]=this.Table[j][i];
                        this.Table[j][i]=0;
                        j--;
                    }else if(this.Table[k][i]==this.Table[j][i]){
                        this.Table[k][i]+=this.Table[j][i];
                        addCachedPoint(this.Table[k][i]);
                        this.Table[j][i]=0;
                        k--;
                        j--;
                    }else if(this.Table[k][i]!=this.Table[j][i]){
                        if(k-1!=j){
                            this.Table[k-1][i]=this.Table[j][i];
                            this.Table[j][i]=0;
                        }
                        k--;
                        j--;
                    }
                }
            }
        }
    }

    /**
     *    apply left (a) action
     */
    private void actionLeft() {
        for(int i=0;i<4;i++){
            int k=0;
            int j=1;
            while(j<4&&k<4){
                if(this.Table[i][j]==0){
                    j++;
                }else if(this.Table[i][j]!=0){
                    if(this.Table[i][k]==0){
                        this.Table[i][k]=this.Table[i][j];
                        this.Table[i][j]=0;
                        j++;
                    }else if(this.Table[i][k]==this.Table[i][j]){
                        this.Table[i][k]+=this.Table[i][j];
                        addCachedPoint(this.Table[i][k]);
                        this.Table[i][j]=0;
                        k++;
                        j++;
                    }else if(this.Table[i][k]!=this.Table[i][j]){
                        if(k+1!=j){
                            this.Table[i][k+1]=this.Table[i][j];
                            this.Table[i][j]=0;
                        }
                        k++;
                        j++;
                    }
                }
            }
        }
    }

    /**
     *    apply right (d) action
     */
    private void actionRight() {
        for(int i=0;i<4;i++){
            int k=3;
            int j=2;
            while(j>=0&&k>=0){
                if(this.Table[i][j]==0){
                    j--;
                }else if(this.Table[i][j]!=0){
                    if(this.Table[i][k]==0){
                        this.Table[i][k]=this.Table[i][j];
                        this.Table[i][j]=0;
                        j--;
                    }else if(this.Table[i][k]==this.Table[i][j]){
                        this.Table[i][k]+=this.Table[i][j];
                        addCachedPoint(this.Table[i][k]);
                        this.Table[i][j]=0;
                        k--;
                        j--;
                    }else if(this.Table[i][k]!=this.Table[i][j]){
                        if(k-1!=j){
                            this.Table[i][k-1]=this.Table[i][j];
                            this.Table[i][j]=0;
                        }
                        k--;
                        j--;
                    }
                }
            }
        }
    }

    /**
     * random filling a non-zero box of the board
     */
    private void randomFill(){
        ArrayList<Integer> xIndex=new ArrayList<>();
        ArrayList<Integer> yIndex=new ArrayList<>();
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                if(this.Table[i][j]==0){
                    xIndex.add(i);
                    yIndex.add(j);
                }
            }
        }
        int randomIndex= (int) Math.floor(Math.random()*xIndex.size());
        int randomNumber= (int) Math.floor(Math.random()*2+1);
        if(xIndex.size()!=0&&yIndex.size()!=0){
            this.Table[xIndex.get(randomIndex)][yIndex.get(randomIndex)]= (int) Math.pow(2,randomNumber);
        }
    }

    /**
     * this function prints player information in header of console including player name ,player score ,
     * cached score in last round and maximum score
     */
    private void printHeader(){
        System.out.println("▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧");
        System.out.println("Player name : "+this.PlayerName);
        System.out.println("Player score : "+this.point +"  ("+this.cachedPoint+")");
        System.out.println("Max score : "+this.maxPoint);
        System.out.println("▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧");
        System.out.println();
    }

    /**
     * this function updates player point (Score)....
     */
    private void updatePoint(){
        this.point+=this.cachedPoint;
    }

    /**
     * this function updates maxPoint if player break it
     */
    private void updateMaxPoint(){
        if(this.point>this.maxPoint){
            this.maxPoint=this.point;
        }
    }

    /**
     * this function extracts MaxPoint
     * @return returns maxPoint
     */
    private int extractMaxPoint(){
        new File("../Save2048").mkdirs();
        String path="../Save2048/MaxScore.txt";
        try {
            FileReader fr=new FileReader(path);
            BufferedReader bufferedReader=new BufferedReader(fr);
            return bufferedReader.read();
        }catch (Exception ex){
            try{
                FileWriter fr=new FileWriter(path);
                BufferedWriter bufferedWriter=new BufferedWriter(fr);
                bufferedWriter.write(0);
                bufferedWriter.close();
                fr.close();
                FileReader fr1=new FileReader(path);
                BufferedReader bufferedReader=new BufferedReader(fr1);
                return bufferedReader.read();
            }catch (Exception exc){
                System.out.println("an error in loading maxPoint");
            }
        }
        return -1;
    }

    /**
     * this function writes player score on maxScore if player break it...
     */
    private void  writeMaxPoint(){
        String path="../Save2048/MaxScore.txt";
        try{
            FileWriter fr=new FileWriter(path);
            BufferedWriter bufferedWriter=new BufferedWriter(fr);
            bufferedWriter.write(this.maxPoint);
            bufferedWriter.close();
            fr.close();
        }catch (Exception ex){
            try{
                FileWriter fr=new FileWriter(path);
                BufferedWriter bufferedWriter=new BufferedWriter(fr);
                bufferedWriter.write(this.maxPoint);
            }catch(Exception exe){
                System.out.println("error in saving file...");
            }
        }
    }

    /**
     * this function saves game
     * @param fileName the name of file
     */
    private void save(String fileName){
        String path="../Save2048/"+fileName+".txt";
        try{
            FileWriter fr=new FileWriter(path);
            BufferedWriter bufferedWriter=new BufferedWriter(fr);
            bufferedWriter.write(this.PlayerName);
            bufferedWriter.write('\n');
            bufferedWriter.write(this.point);
            bufferedWriter.write(this.cachedPoint);
            for(int i=0;i<4;i++){
                for(int j=0;j<4;j++){
                    bufferedWriter.write(this.Table[i][j]);
                }
            }
            //save prevTable
            for(int i=0;i<4;i++){
                for(int j=0;j<4;j++){
                    bufferedWriter.write(this.prevTable[i][j]);
                }
            }
            //
            bufferedWriter.close();
            fr.close();
        }catch (Exception ex){
            System.out.println(ex);
        }
    }

    /**
     * this function loads game
     * @param fileName the name of file
     * @return a player object
     */
    public static Player load(String fileName){
        String path="../Save2048/"+fileName+".txt";
        try{
            FileReader fr=new FileReader(path);
            BufferedReader bufferedReader=new BufferedReader(fr);
            String playerName=bufferedReader.readLine();
            int point=bufferedReader.read();
            int cachedPoint=bufferedReader.read();
            int[][] array=new int[4][4];
            for(int i=0;i<4;i++){
                for(int j=0;j<4;j++){
                    array[i][j]=bufferedReader.read();
                }
            }
            //load prevTable
            int[][] prevArray=new int[4][4];
            for(int i=0;i<4;i++){
                for(int j=0;j<4;j++){
                    prevArray[i][j]=bufferedReader.read();
                }
            }
            //
            bufferedReader.close();
            fr.close();
            return new Player(playerName,array,prevArray,point,cachedPoint);
        }catch (Exception ex){
            System.out.println(ex);
        }
        return null;
    }

    /**
     * this function prints welcome message
     */
    public static void  welcome(){
        System.out.print("\n\n\n");
        System.out.println("▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧");
        System.out.print("\n\n");
        System.out.println("    Ⓦⓔⓛⓒⓞⓜⓔ To 2048    "+"\n\n");
        System.out.println("▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧");
        try{
            TimeUnit.SECONDS.sleep(6);
        }catch (Exception ex){
            System.out.println(ex);
        }
    }

    /**
     * this function prints menu
     */
    public static void menu(){
        System.out.println("▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧");
        System.out.print("         menu          ");
        System.out.print("\n\n\n");
        System.out.println("     Start new game (s)"+"\n");
        System.out.println("     Load game (l)     "+"\n");
        System.out.println("     Exit (e)          "+"\n");
        System.out.println("     About (a)          "+"\n\n");
        System.out.println("▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧");
    }

    /**
     * prints creator information
     */
    public static void about(){
        System.out.println("▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧");
        System.out.print("         About          ");
        System.out.print("\n\n\n");
        System.out.println("  created by Erfan Karami  "+"\n");
        System.out.print("  Student id :98222079     ");
        System.out.print("\n\n");
        System.out.println(" https://gitlab.com/E.karami ");
        System.out.println("▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧");
        System.out.println("    back to menu (b)        ");
        Scanner in=new Scanner(System.in);
        char character;
        do{
            character=in.nextLine().charAt(0);
        }while (character!='b');
    }

    /**
     * prints help section in game page
     */
    private void helpSectionMessage(){
        System.out.println("\n"+"Up:w             Down:s   ");
        System.out.println("Left:a           Right:d  ");
        System.out.println("Save:z      beck to menu:q");
    }

    /**
     * prints game over message
     */
    private void gameOverMessage(){
        System.out.println("▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧");
        System.out.print("\n");
        System.out.print("        GAME OVER !         ");
        System.out.print("\n\n");
        System.out.println("▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧");
        System.out.println("    back to menu (b)        ");
        Scanner in=new Scanner(System.in);
        char character;
        do{
            character=in.nextLine().charAt(0);
        }while (character!='b');
    }
    /**
     * prints win message
     */
    private void winMessage(){
        System.out.println("▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧");
        System.out.print("\n");
        System.out.print("         YOU WIN !         ");
        System.out.print("\n\n");
        System.out.println("▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧");
        System.out.println("    back to menu (b)        ");
        Scanner in=new Scanner(System.in);
        char character;
        do{
            character=in.nextLine().charAt(0);
        }while (character!='b');
    }

    /**
     * clearing console :)
     */
    public static void cls(){
        for(int i=0;i<40;i++){
            System.out.println("\b");
        }
    }

    /**
     * this function updates prevTable after every round
     */
    private void updatePrevTable(){
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                this.prevTable[i][j]=this.Table[i][j];
            }
        }
    }

    /**
     * this function checks that prevTable is like Table or not
     * @return if prevTable and table are similar returns true else returns false
     */
    private boolean isPrevLikeRecent(){
        for (int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                if (this.Table[i][j]!=this.prevTable[i][j]){
                    return false;
                }
            }
        }
        return true;
    }
}
