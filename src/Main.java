import java.util.Scanner;
import java.util.concurrent.TimeUnit;
public class Main {
    public static void main(String[] args){
        Player.welcome();
        Scanner in;
        char character;
        do{
            in=new Scanner(System.in);
            Player.cls();
            Player.menu();
            char validation;
            try{
                validation=in.nextLine().charAt(0);
            }catch (Exception ex){
                validation='m';
            }
            if(validation=='s'||validation=='l'||validation=='a'||validation=='e'){
                character=validation;
            }else{
                character='m';
            }
            switch (character){
                case 's':{
                    Player.cls();
                    System.out.print("Please enter your name (it is less than 20 character) : ");
                    String name=in.next();
                    if(name.length()>20){
                        name=name.substring(0,21);
                    }
                    Player newPlayer=new Player(name);
                    Player.cls();
                    newPlayer.start('s');
                }
                break;
                case 'l':{
                    Player.cls();
                    System.out.print("Please enter save name : ");
                    String name=in.next();
                    try {
                        Player newPlayer=Player.load(name);
                        Player.cls();
                        newPlayer.start('l');
                    }catch (Exception ex){
                        System.out.print("file not have exist or is cropped...");
                        try{
                            TimeUnit.SECONDS.sleep(3);
                        }catch (Exception exception){
                            System.out.println(ex);
                        }
                    }
                }
                break;
                case 'a':{
                    Player.cls();
                    Player.about();
                }
                break;
                case 'e':{/*null*/}
            }
            in=null;
        }while (character!='e');
    }
}
